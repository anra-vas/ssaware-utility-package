import json
import os

from ssaware_utility_package.auth_service.services_abstract_base_class import ServicesAbstractBaseClass
from ssaware_utility_package.web_client.http_status_codes import HTTP_200_OK


class UserServicesBaseClass(ServicesAbstractBaseClass):
    list_users_url: str = 'auth/admin/realms/ANRA/users?first=0&max=999999999'

    def __filter_out_system_users(self, users_list: list) -> list:
        return list(filter(
            lambda u: u and u.get('email') != os.getenv('OAUTH_SERVER_USERNAME'), users_list))

    def list_users(self) -> list:
        response = self.oauth_client.get(
            self.list_users_url
        )
        assert response.status_code == HTTP_200_OK, \
            "Error retrieving users from oauth service"
        return self.__filter_out_system_users(json.loads(response.content.decode()))
