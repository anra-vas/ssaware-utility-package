import json
import os

import jwt
import requests
from jwt import ExpiredSignatureError
from requests import Response

from ssaware_utility_package.auth_service.services_abstract_base_class import ServicesAbstractBaseClass
from ssaware_utility_package.cache_service.cache import Cache
from ssaware_utility_package.web_client.http_status_codes import HTTP_400_BAD_REQUEST, HTTP_200_OK, \
    HTTP_401_UNAUTHORIZED


class OauthServerClientServiceBaseClass(ServicesAbstractBaseClass):
    OAUTH_SERVER_MASTER_TOKEN_CACHE_KEY: str = 'oauth_master_server_auth_token'

    def _ensure_master_user_access_token(self) -> dict:
        existing_token: dict = Cache.get(self.OAUTH_SERVER_MASTER_TOKEN_CACHE_KEY)
        if not existing_token:
            return self._request_new_token()
        new_token: dict = self._refresh_token(existing_token.get('refresh_token'))
        Cache.set(self.OAUTH_SERVER_MASTER_TOKEN_CACHE_KEY, new_token)
        if self._is_token_valid():
            return new_token
        new_token: dict = self._request_new_token()
        Cache.set(self.OAUTH_SERVER_MASTER_TOKEN_CACHE_KEY, new_token)
        return new_token

    def _get_valid_token(self, token_cache_key: str = '') -> str:
        cache_key: str = token_cache_key or self.OAUTH_SERVER_MASTER_TOKEN_CACHE_KEY
        token: str = (Cache.get(cache_key) or {}).get('access_token')
        if token and self._is_token_valid(cache_key):
            return token
        if cache_key == self.OAUTH_SERVER_MASTER_TOKEN_CACHE_KEY:
            return self._ensure_master_user_access_token().get('access_token')
        if not token:
            return ''
        token_data: dict = self._refresh_token(
            refresh_token=(Cache.get(cache_key) or {}).get('refresh_token')
        )
        Cache.set(cache_key, token_data)
        if self._is_token_valid(cache_key):
            return token_data.get('access_token')
        return ''

    def _decode_token(self, token: str, verify=True) -> dict:
        not_verified_token: dict = jwt.decode(token, algorithms='RS256', options={"verify_signature": False})
        key = os.getenv('ANRA_OAUTH_PUBLIC_KEY')
        decoded_token: dict = jwt.decode(token, key=key, algorithms='RS256',
                                         verify=verify, audience=not_verified_token.get('aud', []))
        return decoded_token

    def _refresh_token(self, refresh_token: str) -> dict:
        if not refresh_token:
            return {}
        payload: dict = {
            "client_id": os.getenv('OAUTH_SERVER_CLIENT_ID'),
            "grant_type": 'refresh_token',
            "refresh_token": refresh_token
        }
        oauth_config: dict = self._get_config()
        response: Response = requests.post(oauth_config.get('token_endpoint'),
                                           data=payload,
                                           headers={
                                               'Content-Type': 'application/x-www-form-urlencoded',
                                           }, verify=False)
        if response.status_code == HTTP_400_BAD_REQUEST:
            return {}
        assert response.status_code == HTTP_200_OK, 'Error making refresh token request'
        return json.loads(response.content.decode())

    def _is_token_valid(self, token_cache_key: str = '') -> bool:
        cache_key: str = token_cache_key or self.OAUTH_SERVER_MASTER_TOKEN_CACHE_KEY
        token_data: dict or None = Cache.get(cache_key)
        if not token_data:
            return False
        try:
            self._decode_token(token_data.get('access_token'))
        except ExpiredSignatureError as ex:
            return False
        except Exception as e:
            raise Exception('Error decoding oauth public key')
        return True

    def _request_new_token(self, request_data: dict or None = None) -> dict:
        oauth_server_request_data: dict = request_data or {
            "username": os.getenv('OAUTH_SERVER_USERNAME'),
            "password": os.getenv('OAUTH_SERVER_PASSWORD'),
            "grant_type": os.getenv('OAUTH_SERVER_GRANT_TYPE'),
            "client_id": os.getenv('OAUTH_SERVER_CLIENT_ID')
        }
        oauth_config: dict = self._get_config()
        response: Response = requests.post(oauth_config.get('token_endpoint'),
                                           data=oauth_server_request_data,
                                           headers={
                                               'Content-Type': 'application/x-www-form-urlencoded',
                                           }, verify=False)
        if response.status_code == HTTP_401_UNAUTHORIZED:
            return {}
        assert response.status_code == HTTP_200_OK, "Oauth server get token error."
        response_data: dict = json.loads(response.content.decode())
        return response_data

    def _get_config(self) -> dict:
        config_cache_key: str = 'oauth_config_cache_key'
        config_data: dict or None = Cache.get(config_cache_key)
        if config_data:
            return config_data
        config_response: Response = requests.get(
            os.getenv('OAUTH_SERVER_URL') + "auth/realms/ANRA/.well-known/openid-configuration",
            verify=False
        )
        assert config_response.status_code == HTTP_200_OK, "Error retrieving oauth config"
        config_data: dict = json.loads(config_response.content.decode())
        Cache.set(config_cache_key, config_data, 7200)
        return config_data
