import os
from requests import Session

from ssaware_utility_package.oauth_server_client_services.oauth_server_client_service_base_class import \
    OauthServerClientServiceBaseClass
from ssaware_utility_package.oauth_server_client_services.oauth_session import OauthSession


class GetOauthServerClientService(OauthServerClientServiceBaseClass):
    def get(self) -> Session:
        return self.__make_client()

    def __make_client(self) -> Session:
        s = OauthSession(os.get("OAUTH_SERVER_URL"))
        valid_token: str = self._get_valid_token()
        s.headers.update({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer %s' % valid_token,
        })
        s.verify = False
        return s
