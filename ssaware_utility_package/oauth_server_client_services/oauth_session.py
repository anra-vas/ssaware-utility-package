from urllib.parse import urljoin

from requests import Session


class OauthSession(Session):
    def __init__(self, prefix_url: str):
        self.prefix_url = prefix_url
        super().__init__()

    def request(self, method, url, *args, **kwargs):
        url = urljoin(self.prefix_url, url)
        return super().request(method, url, *args, **kwargs)
