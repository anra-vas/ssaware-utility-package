import os
from ssaware_utility_package.exceptions.api.api_exception import AuthenticationFailed
from ssaware_utility_package.oauth_server_client_services.oauth_server_client_service_base_class import \
    OauthServerClientServiceBaseClass


class AuthService(OauthServerClientServiceBaseClass):

    def authenticate(self, request: object) -> None:
        if request.META.get('HTTP_AUTHORIZATION', ''):
            oauth_token: iter = iter(
                reversed(request.META.get('HTTP_AUTHORIZATION', '').split(' ')))

            oauth_token_decoded: dict = self._decode_token(next(oauth_token, ''))
            setattr(request, 'oauth_authenticated_user_scopes', oauth_token_decoded.get('scope', '').split(' '))
            setattr(request, 'oauth_authenticated_user_pk', oauth_token_decoded['sub'])
            ss_ctr: list = oauth_token_decoded.get('resource_access', {}).get('SS-CTR', {}).get('roles', [])
            mission_manager: list = oauth_token_decoded.get('resource_access', {}).get('MISSION-MANAGER',
                                                                                       {}).get(
                'roles', [])
            setattr(request, 'oauth_authenticated_user_roles', list(set(ss_ctr + mission_manager)))
            return None
        if request.META.get('HTTP_INTERNAL_TOKEN', '') == os.getenv('INTERNAL_APPS_TOKEN'):
            return None
        raise AuthenticationFailed()

    def authenticate_header(self, request) -> str:
        return 'Bearer realm="example", error="invalid_token"'
