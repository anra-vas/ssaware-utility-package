from abc import ABC


class ServicesAbstractBaseClass(ABC):

    @property
    def oauth_client(self):
        from ssaware_utility_package.oauth_server_client_services.get_oauth_server_client_service import \
            GetOauthServerClientService
        return GetOauthServerClientService().get()

    def _get_user_on_oauth_server(self, user_email: str, validate: bool = True, user_id: str = '') -> dict or None:
        from ssaware_utility_package.oauth_server_client_services.user.user_services_base_class import \
            UserServicesBaseClass
        users_data: list = UserServicesBaseClass().list_users()
        user_list: list = list(
            filter(
                lambda u: (u.get('email') if not user_id else u.get('id')) == (user_email if not user_id else user_id),
                users_data))
        if not validate:
            return next(iter(user_list), None)
        if not user_list:
            assert len(user_list) == 1, "No user found on oauth server for: %s" % user_email if not user_id else user_id
        assert len(user_list) == 1, "Only one user should exist on oauth server for given email"
        user: dict = next(iter(user_list), None)
        assert user, "User not found on oauth server"
        return user
