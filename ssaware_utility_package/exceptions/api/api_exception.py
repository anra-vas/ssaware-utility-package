from ssaware_utility_package.web_client.http_status_codes import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_403_FORBIDDEN, \
    HTTP_401_UNAUTHORIZED


class APIException(Exception):
    """
    Base class for REST framework exceptions.
    Subclasses should provide `.status_code` and `.default_detail` properties.
    """
    status_code = HTTP_500_INTERNAL_SERVER_ERROR
    default_detail = 'A server error occurred.'
    default_code = 'error'

    def __init__(self, detail=None):
        if detail is None:
            detail = self.default_detail
        self.detail = detail

    def __str__(self):
        return str(self.detail)


class PermissionDenied(APIException):
    status_code = HTTP_403_FORBIDDEN
    default_detail = 'You do not have permission to perform this action.'
    default_code = 'permission_denied'


class AuthenticationFailed(APIException):
    status_code = HTTP_401_UNAUTHORIZED
    default_detail = 'Incorrect authentication credentials.'
    default_code = 'authentication_failed'
