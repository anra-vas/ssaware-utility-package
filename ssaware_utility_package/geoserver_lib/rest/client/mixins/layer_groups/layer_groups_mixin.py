from typing import List

from requests import Response

from ssaware_utility_package.geoserver_lib.rest.client.mixins.mixins_base_class import MixinsBaseClass


class LayerGroupsMixin(MixinsBaseClass):
    def list_layer_groups(self):
        layer_groups: list = []
        for ws in self.get_workspaces():
            ly_groups = self.get_layergroups(ws.name)
            for ly_group in ly_groups:
                layer_groups.append(ly_group)
        for ly_group in self.get_layergroups():
            layer_groups.append(ly_group)
        return layer_groups

    def __create_layer_group_data_xml(self, workspace: str, layers: List[dict],
                                      layer_group_name: str):
        layers_element: str = ''
        for l in layers:
            layers_element += "<published type=\"layer\"><name>%s</name></published>\n" % l['name']
        return """
        <layerGroup>
<name>%s</name>
<mode>SINGLE</mode>
<workspace>
<name>%s</name>
</workspace>
<publishables>
%s
</publishables>
<styles>
  </styles>
</layerGroup>
        """ % (layer_group_name, workspace, layers_element)

    def create_layer_group_in_workspace(self, workspace: str, layers: List[dict],
                                        layer_group_name: str) -> str:

        if layer_group_name in tuple(
                map(lambda g: getattr(g, 'name'), self.list_layer_groups())):
            return layer_group_name
        _lg_data = self.__create_layer_group_data_xml(workspace, layers, layer_group_name)
        response: Response = self.session.post(
            self._service_url + 'workspaces/%s/layergroups' % workspace,
            data=_lg_data, headers={
                'Content-type': 'application/xml'
            })
        assert response.status_code == 201, "Failed to create layer group"
        return layer_group_name

    def update_layer_group_in_workspace(self, workspace: str, layers: List[dict],
                                        layer_group_name: str, old_layer_group_name: str) -> str:
        _lg_data = self.__create_layer_group_data_xml(workspace, layers, layer_group_name)
        response: Response = self.session.put(
            self._service_url + 'workspaces/%s/layergroups/%s' % (workspace, old_layer_group_name),
            data=_lg_data, headers={
                'Content-type': 'application/xml'
            })
        assert response.status_code == 200, getattr(response, 'reason', "Failed to update layer group")
        return layer_group_name

    def delete_layer_group_in_workspace(self, workspace: str, layer_group_name: str):
        raise NotImplemented
