import json

from requests import Response

from ssaware_utility_package.geoserver_lib.rest.client.mixins.mixins_base_class import MixinsBaseClass


class LayersMixin(MixinsBaseClass):
    workspaces_endpoint: str = 'workspaces'
    datastores_endpoint: str = 'datastores'

    def __list_feature_types_for_workspace_and_datastore(self, workspace_name: str,
                                                         datastore_name: str) -> list:
        endpoint: str = "%s/%s/%s/%s/featuretypes.json" % (self.workspaces_endpoint, workspace_name,
                                                           self.datastores_endpoint, datastore_name)
        response: Response = self.session.get(self.service_url + endpoint, headers={
            'Content-Type': 'application/json'
        })
        assert response.status_code == 200
        ft_data: list = json.loads(response.content.decode()).get('featureTypes', {}) or {}
        ft_data: list = ft_data.get('featureType', [])
        return list(map(lambda w: w.get('name'), ft_data))

    def __recalculate_bounding_box_and_crs(self, workspace_name: str, datastore: str,
                                           layer_name: str):
        endpoint: str = '%s/%s/%s/%s/featuretypes' % (
            self.workspaces_endpoint, workspace_name, self.datastores_endpoint, datastore
        ) + '/%s.%s?recalculate=nativebbox,latlonbbox' % (
                            layer_name, 'xml')
        request_data: str = '<featureType>' \
                            '<name>%s</name>' \
                            '</featureType>' % layer_name
        request: Response = self.session.put(self.service_url + endpoint, data=request_data,
                                             headers={
                                                 'Content-Type': 'text/xml'
                                             })
        assert request.status_code == 200

    def __add_layer_to_workspace_postgis_store(self, workspace_name: str, datastore: str,
                                               source_table_name: str, layer_name: str,
                                               cql: str = '') -> None:
        if layer_name in self.__list_feature_types_for_workspace_and_datastore(workspace_name,
                                                                               datastore):
            return layer_name
        new_layer_data: str = '<featureType><name>%s</name><nativeName>%s</nativeName>' % (
            layer_name, source_table_name)
        if cql:
            new_layer_data += '<cqlFilter>%s</cqlFilter>' % cql
        new_layer_data += '</featureType>'
        response: Response = self.session.post(self.service_url + '%s/%s/%s/%s/featuretypes' % (
            self.workspaces_endpoint, workspace_name, self.datastores_endpoint, datastore
        ), new_layer_data, headers={'Content-Type': 'text/xml'})
        assert response.status_code == 201
        self.__recalculate_bounding_box_and_crs(workspace_name, datastore, layer_name)
        assert layer_name in self.__list_feature_types_for_workspace_and_datastore(workspace_name,
                                                                                   datastore)

    def publish_layer_from_postgis_store(self, workspace_name: str,
                                         datastore_name: str, source_table_name: str,
                                         layer_name: str, cql: str = '') -> str:
        self.__add_layer_to_workspace_postgis_store(workspace_name, datastore_name,
                                                    source_table_name, layer_name, cql=cql)
        layer_role_name: str = self._get_layer_role_name("%s:%s" % (workspace_name, layer_name))
        self.add_role(layer_role_name)
        for user in self._get_users_for_layer_restrictions():
            self.add_user_to_role(layer_role_name, user)
        layer_rule_name = "%s.%s.r" % (workspace_name, layer_name)
        self.update_layer_rule(layer_rule_name, layer_role_name)
        return layer_name

    def delete_layer_from_postgis_store(self, workspace_name: str,
                                        datastore_name: str, layer_name: str) -> str:
        if layer_name not in self.__list_feature_types_for_workspace_and_datastore(workspace_name,
                                                                                   datastore_name):
            return layer_name
        response: Response = self.session.delete(
            self.service_url + '%s/%s/%s/%s/featuretypes/%s?recurse=true' % (
                self.workspaces_endpoint, workspace_name, self.datastores_endpoint, datastore_name,
                layer_name
            ))
        assert response.status_code == 200, response.content.decode()
        assert layer_name not in self.__list_feature_types_for_workspace_and_datastore(
            workspace_name,
            datastore_name)
        self.delete_layer_rule("%s.%s.r" % (workspace_name, layer_name))
        self.delete_role(self._get_layer_role_name("%s:%s" % (workspace_name, layer_name)))
        return layer_name
