import json

from requests import Response

from ssaware_utility_package.geoserver_lib.rest.client.mixins.mixins_base_class import MixinsBaseClass


class RestSecurityMixin(MixinsBaseClass):
    rest_security_url: str = 'security/acl/rest/'

    def list_rest_security_rules(self) -> dict:
        response: Response = self.session.get(
            self.service_url + self.rest_security_url,
            headers={
                'Content-type': 'application/json'
            }
        )
        self._check_response(response, 200)
        response_data: dict = json.loads(response.content.decode())
        return response_data

    def add_rest_security_rule(self, rule_path: str, rule_string: str) -> bool:
        if rule_path not in self.list_rest_security_rules():
            response: Response = self.session.post(self.service_url + self.rest_security_url,
                                                   data='<rules>'
                                                        '<rule resource="%s">'
                                                        '%s'
                                                        '</rule>'
                                                        '</rules>' % (rule_path, rule_string),
                                                   headers={
                                                       'Content-type': 'application/xml'
                                                   })
            self._check_response(response, 200)
        return True

    def update_rest_security_rule(self, rule_path: str, rule_string: str) -> bool:
        if rule_path not in self.list_rest_security_rules():
            return self.add_rest_security_rule(rule_path, rule_string)
        response: Response = self.session.put(self.service_url + self.rest_security_url,
                                              data='<rules>'
                                                   '<rule resource="%s">'
                                                   '%s'
                                                   '</rule>'
                                                   '</rules>' % (rule_path, rule_string),
                                              headers={
                                                  'Content-type': 'application/xml'
                                              })
        self._check_response(response, 200)
        return True
