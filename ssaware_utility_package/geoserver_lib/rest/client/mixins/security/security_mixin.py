import json

from requests import Response

# todo: list, update, ... service rule are similar functions make - one general
from ssaware_utility_package.geoserver_lib.rest.client.mixins.mixins_base_class import MixinsBaseClass
from ssaware_utility_package.geoserver_lib.services.services_base_class import ServicesBaseClass


class SecurityMixin(MixinsBaseClass):
    def list_services_rules(self) -> dict:
        response: Response = self.session.get(self._build_security_url('acl/services'))
        self._check_response(response, 200)
        return json.loads(response.content.decode())

    def add_service_rule(self, rule_path: str, rule_string: str) -> bool:
        if rule_path not in self.list_services_rules():
            response: Response = self.session.post(self._build_security_url('acl/services'),
                                                   data='<rules>'
                                                        '<rule resource="%s">'
                                                        '%s'
                                                        '</rule>'
                                                        '</rules>' % (rule_path, rule_string),
                                                   headers={
                                                       'Content-type': 'application/xml'
                                                   })
            self._check_response(response, 200)
        return True

    def update_service_rule(self, rule_path: str, rule_string: str) -> bool:
        if rule_path not in self.list_services_rules():
            self.add_service_rule(rule_path, rule_string)
            return True
        response: Response = self.session.put(self._build_security_url('acl/services'),
                                              data='<rules>'
                                                   '<rule resource="%s">'
                                                   '%s'
                                                   '</rule>'
                                                   '</rules>' % (rule_path, rule_string),
                                              headers={
                                                  'Content-type': 'application/xml'
                                              })
        self._check_response(response, 200)
        return True

    def delete_service_rule(self, rule_path: str) -> bool:
        if rule_path in self.list_services_rules():
            response: Response = self.session.delete(
                self._build_security_url('acl/services/%s' % rule_path))
            self._check_response(response, 200)
        return True

    def list_layers_rules(self):
        response: Response = self.session.get(self._build_security_url('acl/layers'))
        self._check_response(response, 200)
        return json.loads(response.content.decode())

    def add_layer_rule(self, rule_path: str, rule_string: str) -> bool:
        if rule_path not in self.list_layers_rules():
            response: Response = self.session.post(self._build_security_url('acl/layers'),
                                                   data='<rules>'
                                                        '<rule resource="%s">'
                                                        '%s'
                                                        '</rule>'
                                                        '</rules>' % (rule_path, rule_string),
                                                   headers={
                                                       'Content-type': 'application/xml'
                                                   })
            self._check_response(response, 200)
        return True

    def update_layer_rule(self, rule_path: str, rule_string: str) -> bool:
        if rule_path not in self.list_layers_rules():
            self.add_layer_rule(rule_path, rule_string)
            return True
        response: Response = self.session.put(self._build_security_url('acl/layers'),
                                              data='<rules>'
                                                   '<rule resource="%s">'
                                                   '%s'
                                                   '</rule>'
                                                   '</rules>' % (rule_path, rule_string),
                                              headers={
                                                  'Content-type': 'application/xml'
                                              })
        self._check_response(response, 200)
        return True

    def delete_layer_rule(self, rule_path: str) -> bool:
        if rule_path in self.list_layers_rules():
            response: Response = self.session.delete(
                self._build_security_url('acl/layers/%s' % rule_path))
            self._check_response(response, 200)
        return True

    def make_ogc_services_allowed_only_for_authenticated_users(self) -> bool:
        for service in ServicesBaseClass.DEFAULT_OGC_SERVICES:
            self.update_service_rule("%s.*" % service, ServicesBaseClass.SERVICE_ROLE_AUTHENTICATED)
        self.update_layer_rule("*.*.r", ServicesBaseClass.SERVICE_ROLE_AUTHENTICATED)
        return True
