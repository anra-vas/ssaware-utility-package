import xmltodict
from requests import Response

from ssaware_utility_package.geoserver_lib.rest.client.mixins.mixins_base_class import MixinsBaseClass


class RolesMixin(MixinsBaseClass):

    def list_roles(self) -> list:
        response: Response = self.session.get(self._build_security_url('roles'))
        self._check_response(response, 200)
        content: str = response.content.decode()
        response_data: dict = xmltodict.parse(content)
        return response_data.get('roles', {}).get('role', [])

    def list_roles_for_user(self, username: str) -> list:
        response: Response = self.session.get(self._build_security_url('roles/user/%s' % username))
        self._check_response(response, 200)
        roles: dict or None = xmltodict.parse(response.content.decode()).get('roles', {})
        roles: str or list = [] if not roles else roles.get('role', [])
        return [roles] if isinstance(roles, str) else roles

    def add_role(self, rolename: str) -> bool:
        if rolename not in self.list_roles():
            response: Response = self.session.post(
                self._build_security_url('roles/role/%s' % rolename))
            self._check_response(response, 201)
        return True

    def delete_role(self, rolename: str) -> bool:
        response: Response = self.session.delete(
            self._build_security_url('roles/role/%s' % rolename))
        self._check_response(response, 200)
        return True

    def add_user_to_role(self, rolename: str, username: str) -> bool:
        if username not in self.list_roles_for_user(username):
            response: Response = self.session.post(
                self._build_security_url(
                    'roles/role/%s/user/%s' % (rolename, username)))
            self._check_response(response, 200)
        return True

    def remove_user_from_role(self, rolename: str, username: str) -> bool:
        if rolename in self.list_roles_for_user(username):
            response: Response = self.session.delete(
                self._build_security_url(
                    'roles/role/%s/user/%s' % (rolename, username)))
            self._check_response(response, 200)
        return True
