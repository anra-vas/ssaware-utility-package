from typing import Any

from requests import Response

from ssaware_utility_package.geoserver_lib.rest.catalog.geoserver_lib_catalog import GeoserverLibCatalog


class ResourceActions(GeoserverLibCatalog):
    def _build_resource_url(self, endpoint: str) -> str:
        return "%s%s/%s" % (self.service_url, 'resource', endpoint)

    def read_resource(self, resource: str) -> Any:
        response: Response = self.session.get(self._build_resource_url(resource))
        assert response.status_code == 200
        return response.content.decode()

    def update_resource(self, resource: str, resource_data: Any) -> bool:
        response: Response = self.session.put(self._build_resource_url(resource), resource_data)
        assert response.status_code == 201
        return True
