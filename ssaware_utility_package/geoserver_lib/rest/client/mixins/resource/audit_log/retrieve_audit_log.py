import re

from ssaware_utility_package.geoserver_lib.rest.client.mixins.resource.resource_actions import ResourceActions


class RetrieveAuditLog(ResourceActions):

    def list_audit_logs(self) -> list:
        content: str = self.read_resource('logs')
        return list(set(re.findall(r"geoserver_audit_\d+_?_?\d+.log", content)))
