import time
from datetime import datetime, timedelta

from ssaware_utility_package.geoserver_lib.rest.client.mixins.resource.resource_actions import ResourceActions


class HandleAuthKeysResource(ResourceActions):

    def get_authkeys_file_content(self) -> str:
        return self.read_resource(
            'security/usergroup/default/authkeys.properties')

    def __handle_key(self, username: str, user_auth_key: str = ''):
        keys_resource: str = self.get_authkeys_file_content()
        lines: list = keys_resource.split("\n")
        _add_key: bool = True
        line_for_removal: str = ''
        for line in lines:
            if '=' in line:
                user_name = line.split('=')[1]
                if username == user_name:
                    line_for_removal = line
                    _add_key = False
                    break
        if user_auth_key and _add_key:
            keys_resource += "%s=%s\n" % (user_auth_key, username)
        else:
            keys_resource = keys_resource.replace(line_for_removal, '')
        lines: list = list(filter(lambda l: l, keys_resource.split("\n")))
        keys_resource: str = "\n".join(lines) + "\n"
        self.update_resource('security/usergroup/default/authkeys.properties',
                             keys_resource.encode())

    def add_key(self, username: str, user_auth_key: str):
        self.__handle_key(username, user_auth_key)
        time.sleep(0.3)
        time_start: datetime = datetime.now()
        while next(iter(self.get_key_for_user(username)), None) != user_auth_key:
            self.__handle_key(username, user_auth_key)
            time.sleep(1.5)
            if datetime.now() - time_start > timedelta(seconds=7):
                raise Exception('User key not updated')

    def remove_key(self, username):
        self.__handle_key(username)

    def get_key_for_user(self, username: str, auth_key_file_content: str = '') -> list:
        keys_resource: str = auth_key_file_content or self.get_authkeys_file_content()
        lines: list = keys_resource.split("\n")
        user_keys: list = list()
        for line in lines:
            if '=' in line and username in line:
                line_data: list = line.split('=')
                user_keys.append(line_data[0])
        return user_keys
