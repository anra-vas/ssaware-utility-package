from ssaware_utility_package.geoserver_lib.rest.client.mixins.mixins_base_class import MixinsBaseClass
from ssaware_utility_package.geoserver_lib.rest.client.mixins.resource.audit_log.retrieve_audit_log import \
    RetrieveAuditLog
from ssaware_utility_package.geoserver_lib.rest.client.mixins.resource.handle_auth_keys_resource import \
    HandleAuthKeysResource


class ResourceMixin(MixinsBaseClass, HandleAuthKeysResource, RetrieveAuditLog):
    pass
