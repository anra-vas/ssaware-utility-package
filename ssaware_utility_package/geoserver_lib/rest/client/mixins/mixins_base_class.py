from geoserver.catalog import FailedRequestError
from requests import Response

from ssaware_utility_package.geoserver_lib.rest.catalog.geoserver_lib_catalog import GeoserverLibCatalog


class MixinsBaseClass(GeoserverLibCatalog):
    def _build_security_url(self, endpoint: str) -> str:
        return "%s%s/%s" % (self.service_url, 'security', endpoint)

    def _check_response(self, response: Response, status_code: int):
        if response.status_code != status_code:
            msg = "Tried to make request to {} \n" \
                  + " but got a {} status code: \n{}"
            raise FailedRequestError(msg.format(
                response.url,
                response.status_code,
                response.content.decode()
            ))
