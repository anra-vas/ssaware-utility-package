from geoserver.layer import Layer


class LayerRestrictions():
    layer_role_appendix: str = '____LAYER__ROLE'

    def _get_layer_role_name(self, layer: str or object) -> str:
        if isinstance(layer, Layer):
            layer_name: str = layer.name.replace(':', '____')
        else:
            layer_name: str = layer.replace(':', '____')
        return "%s%s" % (layer_name, self.layer_role_appendix)

    def _get_users_for_layer_restrictions(self) -> list:
        return list(filter(lambda u: u and u != 'admin', self.list_users()))

    def _get_layers_for_layer_restrictions(self) -> list:
        layers: list = list(map(lambda l: l.name, self.get_layers()))
        layer_groups: list = list(map(
            lambda lg: lg.name if not getattr(lg, 'workspace', None) else "%s:%s" % (
                lg.workspace, lg.name), self.list_layer_groups()))

        return list(set(layers + layer_groups))

    def populate_layer_restrictions(self):
        gs_layers: list = self._get_layers_for_layer_restrictions()
        gs_roles: list = self.list_roles()
        gs_users: list = self._get_users_for_layer_restrictions()
        for layer in gs_layers:
            layer_role_name: str = self._get_layer_role_name(layer)
            if layer_role_name not in gs_roles:
                self.add_role(layer_role_name)
            for user in gs_users:
                self.add_user_to_role(layer_role_name, user)
            if ':' in layer:
                layer_data: iter = iter(layer.split(':'))
                workspace: str = next(layer_data, None)
                layer_name: str = next(layer_data, None)
                layer_rule_name = "%s.%s.r" % (workspace, layer_name)
            else:
                layer_rule_name = "%s.r" % layer
            self.update_layer_rule(layer_rule_name, layer_role_name)
