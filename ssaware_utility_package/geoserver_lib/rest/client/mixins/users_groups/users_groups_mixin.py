import xmltodict
from requests import Response

from ssaware_utility_package.geoserver_lib.rest.client.mixins.mixins_base_class import MixinsBaseClass


class UsersGroupsMixin(MixinsBaseClass):

    def list_users(self) -> list:
        response: Response = self.session.get(self._build_security_url('usergroup/users'))
        self._check_response(response, 200)
        user_data: dict or list = xmltodict.parse(response.content.decode()).get('users', {}).get(
            'user', [])
        user_data: list = user_data if isinstance(user_data, list) else [user_data]
        return list(map(lambda u: u.get('userName', ''), user_data))

    def add_user(self, username: str, password: str, enabled: bool = True,
                 user_auth_key: str = '') -> bool:
        if username not in self.list_users():
            response: Response = self.session.post(self._build_security_url('usergroup/users'),
                                                   data="<user>"
                                                        "<userName>%s</userName>"
                                                        "<password>%s</password>"
                                                        "<enabled>%s</enabled>"
                                                        "</user>" % (
                                                            username,
                                                            password,
                                                            'true' if enabled else 'false'
                                                        ), headers={
                    'Content-type': 'application/xml'
                })
            self._check_response(response, 201)
            gs_layers: list = self._get_layers_for_layer_restrictions()
            for l in gs_layers:
                layer_role_name: str = self._get_layer_role_name(l)
                self.add_user_to_role(layer_role_name, username)
        if user_auth_key:
            self.add_key(username, str(user_auth_key))
        return True

    def delete_user(self, username: str) -> bool:
        if username in self.list_users():
            response: Response = self.session.delete(
                self._build_security_url('usergroup/user/%s' % username))
            self._check_response(response, 200)
        self.remove_key(username)
        return True

    def revoke_user_key_access(self, username) -> bool:
        self.remove_key(username)
        return True
