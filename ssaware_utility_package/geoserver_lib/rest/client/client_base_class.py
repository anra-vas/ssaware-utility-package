from geoserver import settings

from ssaware_utility_package.geoserver_lib.rest.catalog.geoserver_lib_catalog import GeoserverLibCatalog


class ClientBaseClass(GeoserverLibCatalog):

    def __init__(self, service_url='http://localhost:8080/geoserver/rest/',
                 username=settings.DEFAULT_USERNAME,
                 password=settings.DEFAULT_PASSWORD, disable_ssl_certificate_validation=True):
        super().__init__(service_url, username, password, disable_ssl_certificate_validation)
        self.session.verify = not disable_ssl_certificate_validation
