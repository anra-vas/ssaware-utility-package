from ssaware_utility_package.geoserver_lib.rest.client.client_base_class import ClientBaseClass
from ssaware_utility_package.geoserver_lib.rest.client.mixins.layer_groups.layer_groups_mixin import LayerGroupsMixin
from ssaware_utility_package.geoserver_lib.rest.client.mixins.layer_restrictions.layer_restrictions import \
    LayerRestrictions
from ssaware_utility_package.geoserver_lib.rest.client.mixins.layers.layers_mixin import LayersMixin
from ssaware_utility_package.geoserver_lib.rest.client.mixins.resource.resource_mixin import ResourceMixin
from ssaware_utility_package.geoserver_lib.rest.client.mixins.roles.roles_mixin import RolesMixin
from ssaware_utility_package.geoserver_lib.rest.client.mixins.security.rest_security_mixin import RestSecurityMixin
from ssaware_utility_package.geoserver_lib.rest.client.mixins.security.security_mixin import SecurityMixin
from ssaware_utility_package.geoserver_lib.rest.client.mixins.users_groups.users_groups_mixin import UsersGroupsMixin
from ssaware_utility_package.geoserver_lib.services.services_base_class import ServicesBaseClass


class Client(SecurityMixin, RolesMixin, UsersGroupsMixin, ResourceMixin, LayerRestrictions,
             ClientBaseClass, LayersMixin, RestSecurityMixin, LayerGroupsMixin):
    def make_ogc_services_allowed_only_for_authenticated_users(self) -> bool:
        for service in ServicesBaseClass.DEFAULT_OGC_SERVICES:
            self.update_service_rule("%s.*" % service, ServicesBaseClass.SERVICE_ROLE_AUTHENTICATED)
        self.update_layer_rule("*.*.r", ServicesBaseClass.SERVICE_ROLE_AUTHENTICATED)
        self.populate_layer_restrictions()
        self.update_rest_security_rule('/**:GET', 'ROLE_AUTHENTICATED')
        return True
