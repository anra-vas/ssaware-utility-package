class ServicesBaseClass(object):
    DEFAULT_OGC_SERVICES: tuple = ('wfs', 'wms', 'wcs', 'wmts')
    SERVICE_ROLE_AUTHENTICATED = 'ROLE_AUTHENTICATED'
