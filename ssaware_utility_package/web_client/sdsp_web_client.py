import requests


class SdspWebClient(object):
    client = requests

    @staticmethod
    def download_file(url: str, file_destination: str) -> None:
        response: requests.Response = SdspWebClient.client.get(url, stream=True)
        file = open(file_destination, "wb")
        for chunk in response.iter_content(chunk_size=1024):
            file.write(chunk)
        file.close()

    @staticmethod
    def make_get_request(url: str, headers: dict = {}) -> requests.Response:
        response: requests.Response = SdspWebClient.client.get(url, stream=False, verify=False, headers=headers)
        return response
