import datetime


class TimeUtilities(object):

    @staticmethod
    def timestamp_to_datetime(timestamp: int or None) -> datetime or None:
        if timestamp is None:
            return None
        __datetime: datetime = datetime.datetime.fromtimestamp(timestamp)
        return __datetime

    @staticmethod
    def string_to_datetime(time_string: str, time_format: str = '%Y-%m-%dT%H:%M:%S%z',
                           replace_colon_in_z_after_char='') -> datetime or None:
        if not time_string:
            return None
        if replace_colon_in_z_after_char:
            time_string_list: list = time_string.split(replace_colon_in_z_after_char)
            time_string = time_string_list[0] + '+' + time_string_list[1].replace(':', '')
        return datetime.datetime.strptime(time_string, time_format)
