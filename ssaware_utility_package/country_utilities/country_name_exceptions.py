country_name_exceptions: dict = {
    'American Somoa': 'American Samoa',
}


def check_country_name_exception(name: str) -> str:
    return country_name_exceptions.get(name, name)
