import os
from typing import Any


class Call(object):
    def __call__(*args):
        return None


class EmptyCacheService(object):

    def __getattribute__(self, name: str) -> Any:
        return Call()


class CacheBase(object):
    REDIS_SERVICE_ID: str = 'redis'

    SERVICES: tuple = (REDIS_SERVICE_ID,)
    DEFAULT_SERVICE: str = REDIS_SERVICE_ID

    @staticmethod
    def service(service_id: str = REDIS_SERVICE_ID) -> object:
        if service_id == CacheBase.REDIS_SERVICE_ID:
            from django_redis.cache import RedisCache
            return RedisCache(server="redis://%s:%d/%d" % (
                str(os.getenv("REDIS_HOST")),
                int(os.getenv("REDIS_PORT")),
                int(os.getenv("REDIS_DB", 0))
            ), params={})
        return EmptyCacheService()

    @staticmethod
    def get(key: str) -> Any:
        return CacheBase.service().get(key)

    @staticmethod
    def set(key: str, value: Any, timeout: int = 300) -> Any:
        assert key, "Cache key must not be empty"
        assert value, "Cache value must not be empty"
        return CacheBase.service().set(key, value, timeout)

    # todo: for e.g. not every service has clear method, when adding services add another layer ob abstraction

    @staticmethod
    def clear():
        CacheBase.service().clear()

    @staticmethod
    def delete_pattern(pattern):
        service: object = CacheBase.service()
        if getattr(service, 'delete_pattern', None):
            service.delete_pattern(pattern)
            return
        raise Exception("Cache service does not supports cache pattern cleaning")
