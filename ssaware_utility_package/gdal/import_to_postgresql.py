import os

WGS84_EPSG_CODE = 4326


class ImportToPostgresql:
    def import_data(self, data_source_path: str, table_name: str, source_srid: int, db_schema: str) -> None:
        command: str = 'ogr2ogr -f "PostgreSQL" PG:"host=%s user=%s dbname=%s password=%s" ' \
                       '%s -nln %s -skipfailures -nlt GEOMETRY -s_srs EPSG:%s -t_srs EPSG:%d ' \
                       '-ds_transaction -lco GEOMETRY_NAME=geom -lco LAUNDER=NO -lco SCHEMA=%s' % (
                           os.getenv("DB_HOST"), os.getenv("DB_USER"), os.getenv("DB_NAME"),
                           os.getenv("DB_PASSWORD"),
                           data_source_path, table_name,
                           str(source_srid),
                           WGS84_EPSG_CODE,
                           db_schema)
        os.system(command)
