import xml.etree.ElementTree as ET
from owslib.wfs import WebFeatureService


class WFSClient:

    def __init__(self, capabilities_url: str, version: str) -> None:
        self.wfs_client = WebFeatureService(
            url=capabilities_url,
            version=version,
            timeout=30 * 60,
        )

    def list_feature_types(self) -> list:
        return list(self.wfs_client.contents)

    def get_layer_feature_count(self, layer_name: str) -> int:
        feature = self.wfs_client.getfeature(
            typename=layer_name,
            startindex=0,
            maxfeatures=1
        )
        tree: ET.Element = ET.fromstring(feature.read())
        return int(tree.attrib.get('numberMatched', '0'))
