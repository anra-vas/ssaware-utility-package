from typing import Any

from django.db import connection, ProgrammingError


class QueryServicesBaseClass(object):
    def execute_sql_statement(self, statement: str, params: list = []) -> Any:
        row: None = None
        with connection.cursor() as cursor:
            cursor.execute(statement, params) if params else cursor.execute(statement)
            try:
                row = cursor.fetchall()
            except ProgrammingError:
                pass
        return row

    def drop_table(self, db_schema: str, table_name: str) -> None:
        self.execute_sql_statement(
            'DROP TABLE IF EXISTS "%s"."%s"' % (
                db_schema,
                table_name
            ))

    def list_tables(self, db_schema) -> tuple:
        table_names: list = self.execute_sql_statement(
            "SELECT table_name FROM information_schema.tables WHERE table_schema = '%s'" %
            db_schema)
        return tuple(map(lambda t: t[0], table_names))
