import logging
import math
from typing import Optional

NUMBER_OF_DECIMALS = 3


def kilometers_to_miles(val: Optional[float]) -> Optional[float]:
    if val is None:
        return val
    return round(val * 0.62137119223733, NUMBER_OF_DECIMALS)


def kilometers_to_statue_miles(val: Optional[float]) -> Optional[float]:
    return kilometers_to_miles(val)


def kilometers_per_hour_to_miles_per_hour(val: Optional[float]) -> Optional[float]:
    if val is None:
        return val
    return round(val * 0.621371, NUMBER_OF_DECIMALS)


def kilometers_per_hour_to_knots(val: Optional[float]) -> Optional[float]:
    if val is None:
        return val
    return round(val * 0.539957, NUMBER_OF_DECIMALS)


def knots_to_kilometers_per_hour(val: Optional[float]) -> Optional[float]:
    if val is None:
        return val
    return round(val * 1 / 0.539957, NUMBER_OF_DECIMALS)


def degress_celsius_to_degress_fahreheit(val: Optional[float]) -> Optional[float]:
    if val is None:
        return val
    return round((val * 9 / 5) + 32, NUMBER_OF_DECIMALS)


def degress_fahrenheit_to_degress_celsius(val: Optional[float]) -> Optional[float]:
    if val is None:
        return val
    return round((val - 32) * 5 / 9, NUMBER_OF_DECIMALS)


def meters_per_second_to_kilometers_per_hour(val: Optional[float]) -> Optional[float]:
    if val is None:
        return val
    return round(val * 3.6, NUMBER_OF_DECIMALS)


def statue_miles_to_kilometers(val: Optional[float]) -> Optional[float]:
    if val is None:
        return val
    return round(val * 1 / 0.62137119223733, NUMBER_OF_DECIMALS)


def feet_to_meters(val: Optional[int]) -> Optional[int]:
    if val is None:
        return val
    return round(val / 3.28084)


def meters_to_feet(val: Optional[float]) -> Optional[float]:
    if val is None:
        return val
    return round(val * 3.28084, NUMBER_OF_DECIMALS)


def calcululate_el_humidity(air_temp: Optional[float], dew_point: Optional[float]) -> Optional[float]:
    if air_temp is None or dew_point is None:
        return None
    try:
        return round(100 * (math.exp((17.625 * dew_point) / (243.04 + dew_point)) / math.exp(
            (17.625 * air_temp) / (243.04 + air_temp))))
    except Exception as e:
        logging.getLogger('django').error(str(e))
        return None
