import functools

from ssaware_utility_package.exceptions.api.api_exception import PermissionDenied
from ssaware_utility_package.permission_service.check_user_permission_service import CheckUserPermissionService


def check_permission(permission: str):
    def check(func):
        @functools.wraps(func)
        def wrap(*args, **kwargs):
            request = args[1]
            view_set = args[0]
            if CheckUserPermissionService(request, permission).check():
                return func(request=request, self=view_set, **kwargs)
            raise PermissionDenied

        return wrap

    return check
