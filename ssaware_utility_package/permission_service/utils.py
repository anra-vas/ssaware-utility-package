from ssaware_utility_package.cache_service.cache import Cache

ROLES_PERMISSIONS_CACHE_KEY_PREFIX: str = 'roles_permissions___'


def generate_roles_permissins_ck(key: str) -> str:
    return '%s___%s' % (ROLES_PERMISSIONS_CACHE_KEY_PREFIX, key)


def clear_roles_permissions_cache() -> None:
    return Cache.delete_pattern('%s*' % ROLES_PERMISSIONS_CACHE_KEY_PREFIX)
