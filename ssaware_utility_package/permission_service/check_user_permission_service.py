import logging
import os

from requests import Response
from ssaware_utility_package.cache_service.cache import Cache
from ssaware_utility_package.permission_service.utils import generate_roles_permissins_ck
from ssaware_utility_package.web_client.sdsp_web_client import SdspWebClient


class CheckUserPermissionService():

    def __init__(self, request: object, permission: str or list) -> None:
        super().__init__()
        self.request = request
        self.permmission = permission

    def __get_permissions(self):
        try:
            ck: str = generate_roles_permissins_ck(self.request.oauth_authenticated_user_pk)
            if perm := Cache.get(ck):
                return perm
            response: Response = SdspWebClient.make_get_request(os.getenv('PERMISSIONS_API'), headers={
                'Authorization': self.request.META['HTTP_AUTHORIZATION']
            })
            perm: list = response.json().get('permissions')
            Cache.set(ck, perm, int(os.getenv('PERMISSIONS_CACHE_TIMEOUT', 300)))
            return perm
        except Exception as e:
            logging.getLogger('django').error(e)
            return []

    def check(self) -> bool:
        user_permissions: list = self.__get_permissions()
        if 'ADMIN' in user_permissions:
            return True
        if not self.request.META.get('HTTP_AUTHORIZATION', '') and self.request.META.get('HTTP_INTERNAL_TOKEN', ''):
            return True
        if isinstance(self.permmission, str):
            return self.permmission in user_permissions
        else:
            for p in self.permmission:
                if p not in user_permissions:
                    return False
            return True
