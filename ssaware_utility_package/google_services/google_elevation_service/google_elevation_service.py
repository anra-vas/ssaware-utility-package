import json
import os
import requests


class GoogleElevationService(object):

    def __init__(self) -> None:
        super().__init__()
        self.elevation_service_url: str = 'https://maps.googleapis.com/maps/api/elevation/json'
        self.elevation_service_api_key: str = os.getenv('GOOGLE_ELEVATION_SERVICE_API_KEY', '')
        self.no_data_value: int = int(os.getenv('ELEVATION_NO_DATA_VALUE'))

    def get_elevations(self, locations: list) -> list:
        try:
            if not self.elevation_service_api_key:
                return [[l[0], l[1], self.no_data_value, self.no_data_value] for l in locations]
            locations_string: str = ""
            for lc in locations:
                locations_string += str(lc[1]) + ',' + str(lc[0]) + '|'
            response: requests.Response = requests.get(
                '%s?key=%s&locations=%s' % (
                    self.elevation_service_url,
                    self.elevation_service_api_key,
                    locations_string.rstrip('|')
                )
            )
            if not 200 <= response.status_code <= 299:
                return [[l[0], l[1], self.no_data_value, self.no_data_value] for l in locations]
            results: dict = json.loads(response.content.decode())
            if not results.get('results'):
                return [[l[0], l[1], self.no_data_value, self.no_data_value] for l in locations]

            for _k, _l in enumerate(locations):
                elevation: float = results['results'][_k]['elevation']
                resolution: float = results['results'][_k]['resolution']
                locations[_k].append(elevation if elevation >= 0 else 0)
                locations[_k].append(resolution)
            return locations
        except Exception:
            return [[l[0], l[1], self.no_data_value, self.no_data_value] for l in locations]

    def get_elevation(self, lat: float, lng: float) -> float:
        try:
            if not self.elevation_service_api_key:
                return self.no_data_value
            response: requests.Response = requests.get(
                '%s?key=%s&locations=%f,%f' % (
                    self.elevation_service_url,
                    self.elevation_service_api_key,
                    lat, lng
                )
            )
            if not 200 <= response.status_code <= 299:
                return self.no_data_value
            results: dict = json.loads(response.content.decode())
            if not results.get('results'):
                return self.no_data_value
            return results['results'][0]['elevation']
        except Exception:
            return self.no_data_value
