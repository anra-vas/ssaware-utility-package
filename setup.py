import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ssaware_utility_package",
    version="0.0.1",
    author="",
    author_email="",
    description="SSAware Utilities Package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/3ppcontractors/ssaware-utility-package/src/master/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.9',
    install_requires=['requests', 'gsconfig-py3', 'xmltodict', 'OWSLib', 'redis', 'pyjwt', 'cryptography',
                      'django-redis'],
)
